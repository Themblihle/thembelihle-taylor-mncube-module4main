import 'package:flutter/material.dart';

import 'package:multipath/models/palette.dart';
import 'package:multipath/screens/dashboard.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool isMale = true;
  bool isLoginScreen = true;
  bool isRememberMe = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        //backgroundColor: const Color.fromARGB(255, 104, 158, 203),
        title: const Text("Login/SignUp"),
        elevation: 4,
        centerTitle: true,
      ),
      body: Stack(children: [
        Positioned(
            //bottom: isLoginScreen ? 200 : 230,
            top: isLoginScreen ? 200 : 230,
            child: Container(
                height: isLoginScreen ? 380 : 250,
                padding: const EdgeInsets.all(20),
                width: MediaQuery.of(context).size.width - 40,
                margin: const EdgeInsets.all(20),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(20),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black.withOpacity(0.3),
                        blurRadius: 15,
                        spreadRadius: 4,
                      )
                    ]),
                child: SingleChildScrollView(
                  child: Column(children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        GestureDetector(
                          onTap: (() {
                            setState(() {
                              isLoginScreen = false;
                            });
                          }),
                          child: Column(
                            children: [
                              Text(
                                "LOGIN",
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold,
                                    color: !isLoginScreen
                                        ? Palette.activeColor
                                        : Palette.textColor1),
                              ),
                              if (!isLoginScreen)
                                Container(
                                  margin: const EdgeInsets.only(top: 3),
                                  height: 2,
                                  width: 55,
                                  color: const Color.fromARGB(255, 0, 187, 255),
                                )
                            ],
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              isLoginScreen = true;
                            });
                          },
                          child: Column(
                            children: [
                              Text(
                                "SIGNUP",
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold,
                                    color: isLoginScreen
                                        ? Palette.activeColor
                                        : Palette.textColor1),
                              ),
                              if (isLoginScreen)
                                Container(
                                  margin: const EdgeInsets.only(top: 3),
                                  height: 2,
                                  width: 55,
                                  color: const Color.fromARGB(255, 0, 187, 255),
                                )
                            ],
                          ),
                        )
                      ],
                    ),
                    if (isLoginScreen) buildSignUp(),
                    if (!isLoginScreen) buildSignIn()
                  ]),
                ))),
        buildBottomContainer(true),
        buildBottomContainer(false),
      ]),
    );
  }

  Container buildSignIn() {
    return Container(
      margin: const EdgeInsets.only(top: 20),
      child: Column(children: [
        buildTextField(Icons.person_outline, "UserName", false, false),
        buildTextField(Icons.lock_outline, "Password", true, false),
        Row(
          children: [
            Checkbox(
                value: isRememberMe,
                activeColor: Palette.textColor2,
                onChanged: (value) {
                  setState(() {
                    isRememberMe = !isRememberMe;
                  });
                }),
            const Text(
              "Remember me",
              style: TextStyle(fontSize: 12, color: Palette.textColor1),
            )
          ],
        )
      ]),
    );
  }

  Container buildSignUp() {
    return Container(
      margin: const EdgeInsets.only(top: 25),
      child: Column(
        children: [
          buildTextField(Icons.person_outline, "First Name", false, false),
          buildTextField(Icons.people_outline, "Last Name", false, false),
          buildTextField(Icons.person_outline, "UserName", false, false),
          buildTextField(Icons.mail_outline, "info@gmail.com", false, true),
          buildTextField(Icons.lock_outline, "Password", true, false),
        ],
      ),
    );
  }

//buttons
  Positioned buildBottomContainer(bool shadows) {
    return Positioned(
      top: isLoginScreen ? 565 : 430,
      right: 0,
      left: 0,
      child: Center(
          child: Column(
        children: [
          ElevatedButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const Dashboard()),
              );
            },
            child: const Text("SUBMIT"),
          )
        ],

        //child: const Icon(
        //Icons.arrow_forward,
        //color: Colors.white,
      )),
    );
  }

  Widget buildTextField(
      IconData icon, String hintText, bool isPassword, bool isEmail) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 8.0),
      child: TextField(
          obscureText: isPassword,
          keyboardType:
              isEmail ? TextInputType.emailAddress : TextInputType.text,
          decoration: InputDecoration(
            prefixIcon: Icon(
              icon,
              color: Palette.iconColor,
            ),
            hintText: hintText,
            enabledBorder: const OutlineInputBorder(
              borderSide: BorderSide(color: Palette.textColor1),
              borderRadius: BorderRadius.all(Radius.circular(25.0)),
            ),
            focusedBorder: const OutlineInputBorder(
              borderSide: BorderSide(color: Palette.textColor1),
              borderRadius: BorderRadius.all(Radius.circular(25.0)),
            ),
            contentPadding: const EdgeInsets.all(10),
            hintStyle: const TextStyle(fontSize: 14, color: Palette.textColor1),
          )),
    );
  }
}
