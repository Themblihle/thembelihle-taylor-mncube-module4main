import 'package:flutter/material.dart';

import 'package:multipath/screens/user_profile.dart';

//import 'package:flutter/src/foundation/key.dart';
//import 'package:flutter/src/widgets/framework.dart';

class FeatureTwo extends StatefulWidget {
  const FeatureTwo({Key? key}) : super(key: key);

  @override
  State<FeatureTwo> createState() => _FeatureTwoState();
}

class _FeatureTwoState extends State<FeatureTwo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Feature Two"),
        centerTitle: true,
      ),
      body: Stack(
        children: [buildDashboardRoute(context)],
      ),
    );
  }

  Positioned buildDashboardRoute(BuildContext context) {
    return Positioned(
        child: Column(
      children: [
        ElevatedButton(
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => const UserProfile()));
            },
            child: const Text("Two")),
      ],
    ));
  }
}
